Emoji Search
---

Created with *create-react-app*. See the [full create-react-app guide](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).



Install
---

`npm install`



Usage
---

`npm start`

---

The following details building and running docker image.

Build
---
`docker build -t registry.gitlab.com/cb.norman/chris-17-01-2022 --build-arg VERSION=$(git rev-parse --short HEAD) .`

Run
---
`docker run -it -p 3000:3000 registry.gitlab.com/cb.norman/chris-17-01-2022`
http://localhost:3000

Push
---
`docker push registry.gitlab.com/cb.norman/chris-17-01-2022`

Deploy in kubernetes
---
enable local k8s in docker if required

deploy using kubectl
```
kubectl apply -f statefulset.yml
kubectl apply -f service.yml
kubectl port-forward service/emoji 8081:80
```
connect to http://localhost:8081/

Security Issues
---
* No proxy is used, nginx or other suitable alternative proxy should be used
* `npm install --production` fails so all development packages are used
* NODE_ENV=production not set seems as though it can cause performance issues, https://www.dynatrace.com/news/blog/the-drastic-effects-of-omitting-node-env-in-your-express-js-applications/

Resources used
---

https://snyk.io/blog/10-best-practices-to-containerize-nodejs-web-applications-with-docker/

TESTING MERGE REQUEST

TODO:
* clean up pipeline and use yaml anchors 
* parse in variables in a nicer way to k8s files for review app, and maybe for production deployment.
* get review app to authenticate and create deployment, i think the auth is wrong in gitlab or missing connect config in the pipeline
* add nginx ingress proxy