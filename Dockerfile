#
# ---- Base Node ----
FROM node:14-alpine AS base
# install node
RUN apk add --no-cache dumb-init curl
# set working directory
WORKDIR /src/emoji
# Set dumb-init as entrypoint
ENTRYPOINT ["dumb-init", "--"]
# copy project file
COPY package.json .

#
# ---- install ----
FROM base AS install
# install node packages
#RUN npm set progress=false && npm config set depth 0
RUN npm install 

#
# ---- Test ----
# run linters, setup and tests
FROM install AS test
COPY . .
RUN  CI=true npm test
#
# ---- Release ----
FROM base AS release

# copy node_modules
COPY --from=install /src/emoji/node_modules ./node_modules
# copy app sources
COPY . .

ARG VERSION="Emoji Search"
RUN sed -i "s/Emoji Search/$(echo ${VERSION})/g" src/Header.js

#use not root user
USER node

# expose port and define CMD
EXPOSE 3000

# add health check
HEALTHCHECK \
    --interval=10s \
    --timeout=5s \
    --start-period=10s \
    --retries=5 \
    CMD curl localhost:3000/emoji-search \
    || exit 1
CMD npm start